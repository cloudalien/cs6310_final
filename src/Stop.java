import java.util.ArrayList;
import java.util.Random;

public class Stop {
    private Integer stopID;
    private String stopName;
    // TODO Passenger list needs to be replaced with actual passengers at some point
    private Double latitude;
    private Double longitude;
    //  stop specific configuration data: stopID, ridersArriveHigh, ridersArriveLow, ridersOffHigh, ridersOffLow, ridersOnHigh, ridersOnLow,
    //  ridersDepartHigh, ridersDepartLow
    private Integer ridersArriveHigh;
    private Integer ridersArriveLow;

    private Integer ridersOffHigh;
    private Integer ridersOffLow;

    private Integer ridersOnHigh;
    private Integer ridersOnLow;

    private Integer ridersDepartHigh;
    private Integer ridersDepartLow;

    //created an Integer variable "waiting" to calculate system efficiency
    private Integer waiting;
    private Integer transfers;


    public Integer getWaiting() {
        return waiting;
    }

    public Stop(String[] inputCmds){
        // System.out.println(" +++ Creating Stop with ID " + inputCmds[1]);

        //input command for a stop follows this standard:
        // add_stop, stopID, stopName,#passengers, latitude, longitude
        this.stopID = Integer.parseInt(inputCmds[1]);
        this.stopName = inputCmds[2];
        this.latitude = Double.parseDouble(inputCmds[4]);
        this.longitude = Double.parseDouble(inputCmds[5]);
        this.waiting = 0;
        this.transfers = 0;

    }

    // parse stop data:
    // stopID, ridersArriveHigh, ridersArriveLow, ridersOffHigh, ridersOffLow, ridersOnHigh, ridersOnLow,
    // ridersDepartHigh, ridersDepartLow
    public Integer updateStopConfig( String[] parameters){
        // given the array of strings that represents a stop config, parse and update variables
        this.ridersArriveHigh = Integer.parseInt(parameters[1]);
        this.ridersArriveLow = Integer.parseInt(parameters[2]);
        this.ridersOffHigh = Integer.parseInt(parameters[3]);
        this.ridersOffLow = Integer.parseInt(parameters[4]);
        this.ridersOnHigh = Integer.parseInt(parameters[5]);
        this.ridersOnLow = Integer.parseInt(parameters[6]);
        this.ridersDepartHigh = Integer.parseInt(parameters[7]);
        this.ridersDepartLow = Integer.parseInt(parameters[8]);
        return 0;
    }

    // used for testing
    public String getStopConfigRidersHighString(){
        String ret = " " + String.valueOf(this.ridersArriveHigh) + " ";
        return ret;
    }
    public Integer exchangePassengers(int initialRiderCount, int busCapacity){

        //System.out.println("Capacity: " + busCapacity);
        int passengersAbleToBoard = 0;

        // Step 1
        //determine passengers that arrive using uniform distribution
        int ridersArrive = getUniformDistributionValue(ridersArriveLow, ridersArriveHigh);

        // update waiting with the arriving passengers
        waiting = waiting + ridersArrive;

        // Step 2

        // determine the number of passengers that get off.
        int ridersOff = Math.max(initialRiderCount - busCapacity, getUniformDistributionValue(ridersOffLow, ridersOffHigh));

        // update transfers group with the number of passengers that get off the bus.
        transfers = transfers + ridersOff;

        // update riders count
        int updatedRidersCount = Math.max(0, initialRiderCount - ridersOff);

        // Step 3
        //determine the number of passengers that get on the bus
        int ridersOn = getUniformDistributionValue(ridersOnLow, ridersOnHigh);

        // update riders and waiting group after riders get on the bus
        int ridersReadyToBoard = updatedRidersCount + ridersOn;

        // calculate number of seats available
        int seatsAvailable = busCapacity - updatedRidersCount;

        //determine passengers that are actually able to board
        if (ridersReadyToBoard > seatsAvailable) {
            passengersAbleToBoard = seatsAvailable;

        } else {
            passengersAbleToBoard = ridersReadyToBoard;
        }

        waiting = Math.max(0, waiting - passengersAbleToBoard);

        // Step 4

        // determine riders that depart
        int ridersDepart = getUniformDistributionValue(ridersDepartLow, ridersDepartHigh);

        if(ridersDepart <= transfers) {
            transfers = transfers - ridersDepart;
            if(transfers > 0)
                waiting = waiting + transfers;
            transfers = 0;
        }
        else{
            int ExtraRidersThatDepart = ridersDepart - transfers;
            waiting = Math.max(0, waiting - ExtraRidersThatDepart);
            transfers = 0;
        }

        //calculate final riders count
        int finalRiderCount = updatedRidersCount + passengersAbleToBoard;
        //System.out.println("Pass Differ: " + (finalRiderCount - initialRiderCount));
        return finalRiderCount - initialRiderCount;
    }

    private int getUniformDistributionValue(int low, int high) {
        int result = 0;
        if(high <= 0 )
            result = low;
        else {
            Random rand = new Random();
            result = rand.ints(low, (high+1)).findFirst().getAsInt();
        }

        return result;
    }
    public Integer getStopID(){
        return this.stopID;
    }


    public Double getLatitude(){return this.latitude;}
    public Double getLongitude(){return this.longitude;}

}
