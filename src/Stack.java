//A fixed size stack that will remove the oldest snapshot and add newer snapshots to the top so that the stack always
//maintains upto 3 most recent snapshots
public class Stack
{
    private EventSnapshot[] stack;
    private int size;
    private int top;

    Stack(int size)
    {
        this.stack = new EventSnapshot[size];
        this.top = -1;
        this.size = size;
    }

    void push(EventSnapshot obj)
    {
        //if stack is full, remove the oldest event
        if (top + 1 >= size){
            dropOldestEvent();
        }

        stack[++top] = obj;
    }

    EventSnapshot pop()
    {
        if (top < 0) throw new IndexOutOfBoundsException();
        EventSnapshot obj = stack[top--];
        stack[top + 1] = null;
        return obj;
    }

    //for debugging purposes only
    EventSnapshot[] peek(){
        return stack;
    }

    public int size()
    {
        return size;
    }

    int elements()
    {
        return top + 1;
    }

    private void dropOldestEvent()
    {
        int index = 0;
        while(index < size - 1){
            stack[index] = stack[index+1];
            index++;
        }
        stack[top--] = null;
    }
}