
public class Event implements  Comparable<Event> {
    // new events should have a type here.
    public static final String move_bus = "move_bus";
    public static final String iterate_system = "itr_sys";
    public static final String change_bus = "change_bus";
    public static final String change_route = "change_route";
    public static final String change_cap = "change_cap";
    public static final String change_speed = "change_speed";

    public Integer timeOfEvent;
    public String  typeOfEvent;
    public Integer objIDeffected;

    public Event(Integer time, String type, Integer objectID){
        this.timeOfEvent = time;
        this.typeOfEvent = type;
        this.objIDeffected = objectID;
    }
    public Event(Event e){ this.timeOfEvent = e.timeOfEvent; this.typeOfEvent = e.typeOfEvent; this.objIDeffected = e.objIDeffected;}

    public Event(String[] inputCmds) {
        //System.out.println(" +++ Creating Event of type " + inputCmds[2]);
        this.timeOfEvent = Integer.parseInt(inputCmds[1]);
        switch (inputCmds[2]){
            case move_bus:
                typeOfEvent = move_bus;
                break;
            default: System.out.println(" ! System does not recognize" + inputCmds[2] + " event type");
        }

        this.objIDeffected  = Integer.parseInt(inputCmds[3]);
    }


    public Integer processMoveBusEvent(Bus b, Integer time_stamp){
        Integer time;
        // where did bus b arrive to, and how long to the next destination?
        // if it's logical time zero, we just started moving, otherwise it has arrived at the "next stop"
        if( b.getIsRunning()){
            b.updateCurrentStop();
        }
        else{
            b.setIsRunning();
        }
        // calculate time to next stop, and generate event
        time = b.getTimeToNextStop();
        time += time_stamp;

        // create new event


        return time;
    }



    @Override
    public int compareTo(Event e){
        return( this.timeOfEvent < e.timeOfEvent ? -1 : (this.timeOfEvent == e.timeOfEvent ) ? 0 :1 );
    }






}
