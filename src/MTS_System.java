import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.*;
import java.util.function.DoubleBinaryOperator;


public class MTS_System {
    public static final String add_depot = "add_depot";
    public static final String add_stop  = "add_stop";
    public static final String add_route = "add_route";
    public static final String extend_route = "extend_route";
    public static final String add_bus   = "add_bus";
    public static final String add_event = "add_event";
    public static final Integer max_moves = 100;

    // The MTS bus system has several associated object that it oversees

    // these objects will be stored in hashmaps since they have object
    // specific ID numbers
    public HashMap<String,Bus> ActiveBuses = new HashMap<String, Bus>();
    private  HashMap<String, Route> ActiveRoutes = new HashMap<String, Route>();
    private HashMap<String, Stop> ActiveStops = new HashMap<String, Stop>();
    private Stack EventSnapshots = new Stack(3);
    private Integer lastProcessedEventLogicalTime = 0;

    private SystemEfficiencyHelper efficiencyHelper = new SystemEfficiencyHelper();

    // Events need to be processed sequentially, so this needs to be ordered.
    private ArrayList<Event> ActiveEvents = new ArrayList<Event>();

    private configFile currentSystemConfigFile;
    private configFile currentStopConfigFile;

    // GUI variables
    private Integer SystemTime = 0;
    private Double SystemEfficiency = 0.0;
    private Double TotalPassengersWaiting = 0.0;
    private Integer NumberOfEventsToRewind = 0;

    // GUI getters
    public Integer getSystemTime(){return SystemTime;}
    public Double getSystemEfficiency(){return SystemEfficiency;}
    public Double getTotalPassengersWaiting(){return TotalPassengersWaiting;}
    public SystemEfficiencyHelper getEfficiencyHelper(){return efficiencyHelper;}
    public Integer getNumberOfEventsToRewind(){return EventSnapshots.elements();}

    public ArrayList<Stop> getActiveStops(){
        ArrayList<Stop> s = new ArrayList<Stop>();

        for(String key :this.ActiveStops.keySet()) {
            s.add( this.ActiveStops.get(key));
        }
        return s;
    }

    public String getRoutesOfStop(Integer stopID){
        ArrayList<Route> l  = new ArrayList<>();
        String temp = "";
        String routeList = "";
        Stop s = this.ActiveStops.get(String.valueOf(stopID));
        for(String key : this.ActiveRoutes.keySet()){
            Route tempR = this.ActiveRoutes.get(key);
            if((tempR.getMyStops()).contains(s) ){
                l.add(tempR);
            }
        }

        for(Route r: l){
            temp += r.getRouteID() +",";
        }

        // remove trailing comma
        routeList = temp.substring(0, temp.length()-1);

        return routeList;
    }

    public ArrayList<Route> getActiveRoutes(){
        ArrayList<Route> r = new ArrayList<Route>();

        for(String key :this.ActiveRoutes.keySet()) {
            r.add( this.ActiveRoutes.get(key));
        }
        return r;
    }
    public ArrayList<Bus> getActiveBuses(){
        ArrayList<Bus> b = new ArrayList<Bus>();

        for(String key :this.ActiveBuses.keySet()) {
            b.add( this.ActiveBuses.get(key));
        }
        return b;
    }
    public Integer getMaxNumberOfEventsToRewind(){

        return EventSnapshots.elements();

    }

    public Bus getBusByID(int busID) {
        return this.ActiveBuses.get(Integer.toString(busID));
    }

    public Route getRouteByID(int routeID) {
        return this.getActiveRoutes().get(routeID);
    }


    public void changeCoefficients(Double kSpeed, Double kCapacity, Double kBuses, Double kWaiting, Double kCombined){
        Scanner input = new Scanner(System.in);
        double value;
        Double tempDouble;

        //get coefficients from user
        System.out.println("Enter your K_SPEED value: " );
        value = input.nextDouble();
        tempDouble = Double.valueOf(value);
        //this.efficiencyHelper.setK_SPEED(tempDouble);

        // from GUI
        this.efficiencyHelper.setK_SPEED(kSpeed);

        System.out.println("Enter your K_CAPACITY value: " );
        value = input.nextDouble();
        tempDouble = Double.valueOf(value);
        //this.efficiencyHelper.setK_CAPACITY(tempDouble);

        // from GUI
        this.efficiencyHelper.setK_CAPACITY(kCapacity);

        System.out.println("Enter your K_BUSES value: " );
        value = input.nextDouble();
        tempDouble = Double.valueOf(value);
        //this.efficiencyHelper.setK_BUSES(tempDouble);

        // from GUI
        this.efficiencyHelper.setK_BUSES(kBuses);

        System.out.println("Enter your K_WAITING value: " );
        value = input.nextDouble();
        tempDouble = Double.valueOf(value);
        //this.efficiencyHelper.setK_WAITING(tempDouble);

        // from GUI
        this.efficiencyHelper.setK_WAITING(kWaiting);

        System.out.println("Enter your K_COMBINED value: " );
        value = input.nextDouble();
        tempDouble = Double.valueOf(value);
        //this.efficiencyHelper.setK_COMBINED(tempDouble);

        // from GUI
        this.efficiencyHelper.setK_COMBINED(kCombined);
    }

    public void calculateSystemEfficiency(){

        Double sum_waiting = 0.0;
        Double sum_bus_cost = 0.0;
        Double systemEfficiency;

        for(String key :this.ActiveStops.keySet()){
            Stop tempStop;
            tempStop = this.ActiveStops.get(key);
            sum_waiting = sum_waiting + tempStop.getWaiting().doubleValue();
        }



        for(String key : this.ActiveBuses.keySet()){
            Bus tempBus;
            tempBus = this.ActiveBuses.get(key);
            Double currentCapacity = tempBus.getInitialPassengerCapacity().doubleValue() - tempBus.getPassengersOnBus().doubleValue();
            sum_bus_cost = sum_bus_cost + (this.efficiencyHelper.getK_SPEED() * tempBus.getSpeedOfBus().doubleValue()) + (this.efficiencyHelper.getK_CAPACITY() * currentCapacity);
        }

        systemEfficiency = this.efficiencyHelper.getK_WAITING() * sum_waiting + this.efficiencyHelper.getK_BUSES() * sum_bus_cost + this.efficiencyHelper.getK_COMBINED() * sum_waiting * sum_bus_cost;

        // to GUI
        SystemEfficiency = this.efficiencyHelper.getK_WAITING() * sum_waiting + this.efficiencyHelper.getK_BUSES() * sum_bus_cost + this.efficiencyHelper.getK_COMBINED() * sum_waiting * sum_bus_cost;
        TotalPassengersWaiting = sum_waiting;

        System.out.println("The system efficiency is " + systemEfficiency);
    }


    private class configFile{
        String configFileName;
        ArrayList<String> configCmds = new ArrayList<String>();
        Iterator<String> cmdItr = null;
        // constructor opens the file and reads in the config lines
        // a configFile's main purpose is to containerize the configuration
        // commands per file, regardless of when they're read in.
        public configFile(String initialConfigFilename){
            this.configFileName = initialConfigFilename;
            BufferedReader inputFile = null;
            try {
                inputFile = new BufferedReader( new FileReader(this.configFileName));
                String line = null;
                while((line = inputFile.readLine()) != null) {
                    configCmds.add(line);
                    //System.out.println(" config: " + line);
                }
            }
            catch(IOException e){
                e.printStackTrace();
            }

            this.cmdItr = configCmds.iterator();
        }

        public String getConfigName(){
            return this.configFileName;
        }

        public String getNext(){
            String tmp;
            if( this.cmdItr.hasNext()) {
                tmp = (this.cmdItr).next();
                return tmp;
            }
            else{
                return null;
            }

        }
    }


    public MTS_System(String systemConfigFile, String stopConfigFile){
        this.currentSystemConfigFile = new configFile(systemConfigFile);
        this.currentStopConfigFile = new configFile(stopConfigFile);

        reset_buses();
    }

    public void reset_buses(){
        String tmpCmd = null;
        String splitBy = ",";
        this.SystemTime = 0;
        this.count = 0;
        System.out.println(" + MTS Bus System according to config file " + this.currentSystemConfigFile.getConfigName());
        this.SystemEfficiency = 0.0;
        this.TotalPassengersWaiting = 0.0;
        this.NumberOfEventsToRewind = 0;
        this.ActiveRoutes.clear();
        this.ActiveEvents.clear();
        this.ActiveBuses.clear();
        this.ActiveStops.clear();


        // for each command in the file, print it, process it.
        while((tmpCmd = this.currentSystemConfigFile.getNext()) != null) {
            String[] cmdParts = tmpCmd.split(splitBy);
            //System.out.println(" + " + tmpCmd);
            switch(cmdParts[0]){
                case add_depot:
                    //System.out.println(" ++ Ignoring add_depot");
                    break;
                case add_stop:
                    this.ActiveStops.put( cmdParts[1], (new Stop(cmdParts)));
                    break;

                case add_route:
                    this.ActiveRoutes.put( cmdParts[1], (new Route(cmdParts)));
                    break;

                case extend_route:
                    // add a reference to an already created stop to the instance of this route.
                    (this.ActiveRoutes.get(cmdParts[1])).extend((this.ActiveStops.get(cmdParts[2])));
                    break;

                case add_bus:
                    // System.out.println(" Bus ID + " + cmdParts[1] + " at stop " + cmdParts[3]);
                    // cmdParts 3 is an index into the route, not the stop ID.
                    this.ActiveBuses.put( cmdParts[1], (new Bus(cmdParts,(this.ActiveRoutes.get(cmdParts[2])) ) ));
                    break;

                case add_event:
                    this.ActiveEvents.add(new Event(cmdParts));
                    break;
                default:
                    System.out.println(" ! Could not parse this config command: " + tmpCmd);
                    break;
            }

        }

        System.out.println(" + Configuring Stops within the system with config file:  " + this.currentStopConfigFile.getConfigName());
        // for each command in the file, print it, process it.
        while((tmpCmd = this.currentStopConfigFile.getNext()) != null) {
            String[] cmdParts = tmpCmd.split(splitBy);
            //System.out.println(" + " + tmpCmd);
            // get stop from stopID
            Stop tmpStop = ActiveStops.get(cmdParts[0]);
            if(tmpStop != null) {
                tmpStop.updateStopConfig(cmdParts);
            }
        }

        // before we go, to set up, all pre-configured events need to be ordered.
        // at the end of every "turn", update the ordering of events.
        Collections.sort(this.ActiveEvents);


    }


    public void summarize(){
        System.out.println(" +++ Summarizing Current Layout of MTS System +++");
        System.out.println(" + Current Routes, with their associated stops: ");

        System.out.println(" + Current Active Events: ");
        for(Event e : this.ActiveEvents){
            System.out.println(" ++ Event @ " + e.timeOfEvent + ": " + e.typeOfEvent + " " + e.objIDeffected);
        }
    }// end of summary method


    private Integer count; // TODO test var, remove
    // iterate the system, check for events
    // in interactive mode, we only want to do one event at a time,
    // but in automatic we process all the events in a timestamp.
    // Int steps - number of events the caller would like to process.
    // this relies on the old events being removed.
    public Integer iterateSystem(Integer steps){

        ArrayList<Event> currentEvents = new ArrayList<Event>();
        ArrayList<Event> eventsToProcess = new ArrayList<Event>();
        Integer moveBusEvents = 0;

        //System.out.println(" ++ Processing system time " + this.SystemTime);
        for (Event e: this.ActiveEvents){
            // process all events with the timestamp
            if( e.timeOfEvent == this.SystemTime ) {

                if( moveBusEvents < steps ){ // are there any change events associated?
                    //System.out.println(" + Found Event affecting object " + e.objIDeffected + " at " + this.SystemTime);
                    if(e.typeOfEvent == Event.move_bus){
                        moveBusEvents++;
                        eventsToProcess.add(e);
                    }
                }
                currentEvents.add(e);
                // we've found an event to process, are there any associated change_bus events?
            }
        }

        // if this timestamp does not have any more events to process in it,
        // iterate the system time
        if( currentEvents.size() == 0 ) {
            this.SystemTime++;
            return 0;
        }

        // TODO remove, debug stopping functionality
        //this.count++;
        //if(this.count++ > 20) return -1;
        // we either care about one obj ID or all of the ones in the timestamp.
        // for the object IDs in the events we're processing, make sure
        // we also include their non-move_bus events.
        if(eventsToProcess.size() != currentEvents.size()) {
            ArrayList<Integer> objIDs = new ArrayList<>();
            for (Event n : eventsToProcess) {
                objIDs.add(n.objIDeffected);
            }

            for (Event c : currentEvents) {
                    // this is terrible but it puts all the config events first so the move_bus
                    // event is processed last. badbad
                    if (objIDs.contains(c.objIDeffected) && c.typeOfEvent != Event.move_bus) {
                        eventsToProcess.add(0, c);
                    }
            }
        }
        // if the list has events in it, process
        //System.out.println(" ++ System: events in Timestamp[" + currentEvents.size() + "], events to process[" + eventsToProcess.size() + "]");
        process_events(eventsToProcess);

        // remove the events we processed from the current list
        currentEvents.removeAll(eventsToProcess);


        return eventsToProcess.size();
    }


    // process events in a list
    // they are assumed to be at the current system time
    public void process_events(ArrayList<Event> currentEvents){
        Integer time;

        // if there are any change_bus events, they have to be managed first.
        // capacity changes affect how many passengers get on and off once the bus reaches the stop.
        // if the route changes, then no passengers can get on? TODO ask

        for(Event e : currentEvents){
            //System.out.println(" + Found " + e.typeOfEvent + " Event affecting object " + e.objIDeffected + " at " + e.timeOfEvent);
            switch(e.typeOfEvent){

                case Event.move_bus:
                    // get effected bus
                    Bus b = this.ActiveBuses.get( String.valueOf(e.objIDeffected));

                    //create deep copy of bus for persisting in snapshot, serialize/deserialize can be used but will be slow
                    Bus busToStoreInEventSnapshot = new Bus(b);

                    // where did bus b arrive to, and how long to the next destination?
                    // if it's logical time zero, we just started moving, otherwise it has arrived at the "next stop"
                    if( b.getIsRunning()){
                        // do passenger exchange
                        // get current passengers on bus
                        int currentPassengersOnBus = b.getPassengersOnBus();

                        // get bus capacity
                        int capacity = b.getInitialPassengerCapacity();

                        // get current stop
                        Stop currentStop = b.getCurrentStop();

                        // get passenger diff after exchange
                        int passengerDiff = currentStop.exchangePassengers(currentPassengersOnBus,capacity);

                        // update passenger count
                        b.updatePassengerCount(passengerDiff);

                    }

                        // returns time of next bus event
                        time = e.processMoveBusEvent(b, this.SystemTime);

                        System.out.print("b:" + e.objIDeffected + "->s:"+b.getNextStop().getStopID()+"@" + time +"//p:" + b.getPassengersOnBus() +"/f:0\n");
                        // create new event
                        Event new_event = new Event(time, Event.move_bus, e.objIDeffected);
                        this.ActiveEvents.add(new_event);

                        // calculate system efficiency
                        calculateSystemEfficiency();

                        //TBD: store bus in snapshot after processing vs before processing
                        this.EventSnapshots.push(new EventSnapshot(new_event, busToStoreInEventSnapshot));

                        //remember the lastProcessedEventLogicalTime to support replay and resume simulation
                        lastProcessedEventLogicalTime = time;
                        break;

                case Event.change_bus:
                    // get effected bus
                    Bus c = this.ActiveBuses.get( String.valueOf(e.objIDeffected));
                    // process change Bus Event
                    // if we have already processed the changes for this system
                    // move on, otherwise we  have to update.
                    if( c.getIsChangePending()){
                        c.processPendingChange();
                    }

                    // calculate system efficiency
                    calculateSystemEfficiency();

                    break;
                default:
                        System.out.println(" ! System Event cannot be processed");
                        break;

            }

        }

        // Remove events we processed
        this.ActiveEvents.removeAll(currentEvents);

        // before we go, to set up, all pre-configured events need to be ordered.
        // at the end of every "turn", update the ordering of events.
        Collections.sort(this.ActiveEvents); // really this doesn't matter right now but it might later.

    }

    public void processCmdParts(String[] cmdParts){
        System.out.println(" +++ cmd to process: " + cmdParts[0]);
        switch (cmdParts[0]){
            // change_bus, route/cap/speed, objID, <args>
            case Event.change_bus: // generate a change bus event to be processed
                // when the bus arrives at the next station
                Bus b = null;
                Integer timeOfChange = 0;
                // Event will be added at correct timestamp, but
                // the handler will take the MOST RECENT BUS CHANGE.
                // there should always be a new event for this bus.
                for (Event e: this.ActiveEvents) {
                    if( e.objIDeffected == Integer.parseInt(cmdParts[2])){
                        timeOfChange = e.timeOfEvent;
                        b = this.ActiveBuses.get( String.valueOf(e.objIDeffected));
                    }
                }
                b.setIsChangePending();
                b.setPendingChange(cmdParts, this.ActiveRoutes);
                Event new_event = new Event(timeOfChange, Event.change_bus, Integer.parseInt(cmdParts[2]));
                this.ActiveEvents.add(new_event);
                break;

            // process events until a bus moves.
            case Event.move_bus:
                while( this.iterateSystem(1) == 0) continue; // this function returns zero if it hasn't found an event.
                break;

            // process events one at a time, not just move_bus
            case Event.iterate_system:
                Integer ret;
                ret = this.iterateSystem(1);
                break;
            default:
                break;
        }

    }

    // once the system is set up, the user will trigger events
    // events can be
    // "move bus", which triggers the next bus move in the event list
    // "change bus", which will change an aspect of the bus listed
    // "system efficiency calculation", which will return the current system efficiency data.
    // "replay events", which will rewind the system back 1 to 3 BUS MOVES
    // TODO: Reset Bus functionality needs to be added
    // TODO: Stop/Startover functionality needs to be added

    public void passNewEvent(String newEvent) {

        String splitBy = ",";
        String[] cmdParts = newEvent.split(splitBy);
        processCmdParts(cmdParts);

    }


    public void listenForEvents(){
        String currentCommand = "";
        String splitBy = ",";
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        // TODO this interface needs to be defined, "quit" will work for now
        while( !currentCommand.contains("quit")){
            try {
                currentCommand = input.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }

            String[] cmdParts = currentCommand.split(splitBy);

            System.out.println(" + " +currentCommand);
            processCmdParts(cmdParts);
        }


    }

    public Integer rewind(Integer numberOfEventsToRewind) {
        System.out.println("\r\n Rewinding " + numberOfEventsToRewind + " events. LastProcessedEventLogicalTime : " + lastProcessedEventLogicalTime);
        LogEventSnapshots("Event snapshots before rewind:");

        if(numberOfEventsToRewind > EventSnapshots.elements()){
            System.out.println("Validation failed. Cannot rewind " + numberOfEventsToRewind + " events. Eventsnapshots available: " + EventSnapshots.elements());
            return lastProcessedEventLogicalTime;
        }

        while(numberOfEventsToRewind > 0){
            EventSnapshot historicalEventSnapshotToRewindTo = EventSnapshots.pop();

            //restore bus state to the way it was before previous event was executed
            this.ActiveBuses.replace(String.valueOf(historicalEventSnapshotToRewindTo.ActiveBus.getBusID()), historicalEventSnapshotToRewindTo.ActiveBus);

            --numberOfEventsToRewind;

            LogEventSnapshots("Eventsnapshots after pop:");
        }

        System.out.println("\r\nSystem rewind complete. Trigger next event to resume simulation. EventSnapshots left to rewind to: " + EventSnapshots.elements());

        return lastProcessedEventLogicalTime;
    }

    private void LogEventSnapshots(String message) {
        System.out.println("\r\n" + message);;
        for(EventSnapshot snapshot: EventSnapshots.peek()){
            if(snapshot != null)
                System.out.print("b:" + snapshot.ProcessedEvent.objIDeffected + "->s:"+snapshot.ActiveBus.getNextStop().getStopID()+"@" + snapshot.ActiveBus.getTimeToNextStop() +"//p:0/f:0\n");
        }
    }
    void LogActiveEvents(String message) {
        System.out.println("\r\n" + message);;
        for(Event event: this.ActiveEvents){
            if(event != null)
                System.out.print("b:" + event.objIDeffected + "@t:"+event.timeOfEvent + "\n");
        }
    }

}
