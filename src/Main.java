import java.io.File;

public class Main {
    private static MTS_System newSystem;
    private static Integer time_stamp = 0;
    private static Integer busesMoved = 0;

    public static void main(String[] args) {

        String[] systemArgs = new String[3];
        // create system
        if( args.length < 3){
            systemArgs[0] = "I";
            systemArgs[1] = "ProjectTesting\\A9_Test_Cases_export\\test20_point_tandem.txt";
            systemArgs[2] = "ProjectTesting\\A9_Test_Cases_export\\test_evening_distibution.csv";
        }
        else{
            systemArgs = args;
        }

        for (Integer itr = 1; itr < systemArgs.length; itr++) {
            File f = new File(systemArgs[itr]);
            if (!f.exists()) {
                System.err.println(" ! Please enter valid configuration file paths for " + systemArgs[itr]);
                return;
            } else {
                System.out.println(" + File(" + systemArgs[itr] + ") valid. ");
            }
        }

            newSystem = new MTS_System(systemArgs[1], systemArgs[2]);
            //newSystem.LogActiveEvents("\r\nEvents on system init, before 1st simulation: ");
            //newSystem.summarize();
            MainWindow mainWindow = new MainWindow(newSystem);
            mainWindow.setVisible(true);

            // pass system to mainwindow, show form
            RunSimulation(systemArgs[0], 20);

            mainWindow.RefreshSimulatorControlPanel();

        //RunSimulation(20);
        //System.out.println("\r\nRunning simulation, moving " + 20 + " buses");
        //RunSimulation(systemArgs[0], 20);

        //UI will pass in number of events to rewind (1, 2, 3)
        //rewind();
    }

    private static void RunSimulation(String mode, int numberOfBusesToMove) {

        if (mode.charAt(0) == 'I') { // Interactive Mode, requires input from the user
            System.out.println(" \n\n + Entered Interactive Mode");
            newSystem.listenForEvents();
        }
        // Automatic Mode, iterates through events until there have been n bus movements
        else if (mode.charAt(0) == 'A')
        {
            // moved 20 buses
            System.out.println("\n\n\n + Beginning Simulation");
            while (busesMoved < numberOfBusesToMove) {
                //System.out.println(" + Buses Moved " + newSystem.getSystemTime() + ": " + busesMoved);
                busesMoved += newSystem.iterateSystem(MTS_System.max_moves);
                if( busesMoved < 0){ // todo remove
                    return;
                }
            }
        }
    }

    private static void rewind() {
        newSystem.LogActiveEvents("Active Events after 1st simulation, before rewind: ");

        //rewind(1)
        time_stamp = newSystem.rewind(1);

        //for now auto resume simulation after rewind by triggering 1 move_bus event, eventually this will be triggered by UI Move_Bus button
        System.out.println("Resuming simulation, auto triggering 1 move_bus event for logical time:" + time_stamp);
        RunSimulation("A",busesMoved+1); //this will add back the latest processed event to snapshots bringing re-filling the stack up;

        newSystem.LogActiveEvents("Events after 2nd simulation, after rewinding/replaying 1: ");

        //rewind(3)
        time_stamp = newSystem.rewind(3);
        System.out.println("Resuming simulation, auto triggering 3 move_bus event for logical time:" + time_stamp);
        RunSimulation("A", busesMoved+3);

        newSystem.LogActiveEvents("Events after 3rd simulation, after rewinding/replaying 3: ");

        //TODO: Various replay scenarios to test
    }
}

