import java.util.ArrayList;
import java.util.HashMap;

public class Bus {
    private Integer busID;
    private Route myRoute;
    private Stop currentStop;
    private Integer currentIndex;
    // TODO current number/objects of passengers to be added here
    private Integer initialPassengerCapacity;
    private Integer passengersOnBus;
    private Integer speedOfBus;
    private Boolean isRunning = Boolean.FALSE;

    private Boolean isChangePending = Boolean.FALSE;
    private Boolean isRoutePending = Boolean.FALSE;
    private Boolean isCapPending = Boolean.FALSE;
    private Boolean isSpeedPending = Boolean.FALSE;
    private Route pendingRoute;
    private Integer pendingStop;
    private Integer pendingCap;
    private Integer ridersOffPending = 0;
    private Integer pendingSpeed = 0;

    // add_bus, ID, Initial Route, Location, Initial Passenger Capacity, Initial Speed
    // also initialize amount of passengers to 1
    public Bus(String[] inputCmds, Route newRoute) {

        //System.out.println(" +++ Creating Bus with ID " + inputCmds[1] + " starting at Stop[" + this.currentIndex +  "] = " + this.currentStop.getStopID());
        // add_bus cmd has the following objects:
        this.busID = Integer.parseInt(inputCmds[1]);
        this.currentStop = newRoute.getStopAtIndex(inputCmds[3]);
        this.currentIndex = Integer.parseInt(inputCmds[3]);
        this.myRoute = newRoute;
        this.initialPassengerCapacity = Integer.parseInt(inputCmds[4]);
        this.speedOfBus = Integer.parseInt(inputCmds[5]);
        this.passengersOnBus = 0;
    }

    public Bus(Bus busToClone) {

        //System.out.println(" +++ Cloning Bus with ID " + busToClone.busID + " starting at Stop[" + this.currentIndex +  "] = " + this.currentStop.getStopID());
        this.busID = busToClone.busID;
        this.myRoute = new Route(busToClone.myRoute);
        this.currentIndex = busToClone.currentIndex;
        this.currentStop = myRoute.getStopAtIndex(String.valueOf(busToClone.currentIndex));
        this.initialPassengerCapacity = busToClone.initialPassengerCapacity;
        this.speedOfBus = busToClone.speedOfBus;
        this.passengersOnBus = busToClone.passengersOnBus;
        this.isRunning = busToClone.isRunning;
    }


    // Getters

    public Integer getBusID(){ return this.busID; }

    public Integer getPassengersOnBus(){return this.passengersOnBus;}

    public Stop getCurrentStop(){ return this.myRoute.getStopAtIndex(String.valueOf(this.currentIndex));}
    public Route getMyRoute(){return this.myRoute;}
    public Stop getNextStop(){return this.myRoute.getNextStop(this.currentIndex);}

    public Boolean getIsRunning(){ return this.isRunning;}
    public Boolean getIsChangePending(){return this.isChangePending;}
    public void setIsChangePending(){ this.isChangePending = Boolean.TRUE;}
    public String getPendingChange(){

        String retval = String.valueOf(this.pendingRoute) + ":" + String.valueOf(this.pendingCap) + ":" + String.valueOf(this.pendingSpeed);
        return retval;
    }

    public void setPendingChange(String cmd[], HashMap<String,Route> r){
        Integer itr;

        // for types of events
        // change_bus, route, busID, route, stop
        switch (cmd[1]){
            // cmd, new capacity,
            case Event.change_cap:
                this.pendingCap = Integer.parseInt(cmd[3]);
                this.isCapPending = Boolean.TRUE;
                break;
            // cmd, new route, new stop
            case Event.change_route:
                this.pendingRoute = r.get(cmd[3]);
                this.pendingStop = Integer.parseInt(cmd[4]);
                this.isRoutePending = Boolean.TRUE;
                break;
            // cmd, new speed
            case Event.change_speed:
                this.pendingSpeed = Integer.parseInt(cmd[3]);
                this.isSpeedPending = Boolean.TRUE;
                break;
            default:
                break;
        }

        return;
    }

    public Integer getRidersOffPending(){return this.ridersOffPending;};

    public void processPendingChange(){

        // speed can just change, it doesn't matter until the bus calculates the next stop
        if(this.isSpeedPending){
            this.speedOfBus = this.pendingSpeed;
            this.isSpeedPending = Boolean.FALSE;
        }

        // capacity will change, keep track of how many people have to get off to accomodate the change.
        if(this.isCapPending){
            if( this.pendingCap < this.initialPassengerCapacity){
                this.ridersOffPending = this.passengersOnBus - this.pendingCap;
            }

            this.initialPassengerCapacity = this.pendingCap;
            this.isCapPending = Boolean.FALSE;
        }

        // if the route is changing, we can't change the current stop because we need it.
        // however, we can change the current route an the current index, as the index into
        // the route is reserved for bus movements.
        if(this.isRoutePending){
            this.myRoute = this.pendingRoute;
            this.currentIndex = this.pendingRoute.getStopIndexWithID(this.pendingStop);
            this.isRoutePending = Boolean.FALSE;
        }

    }


    public Integer getTimeToNextStop(){
        Integer time = 0;
        Stop next = this.myRoute.getNextStop(this.currentIndex);
        Double distance = 0.0;
        // distance is a factor of lat./long for both bus stops.
        distance = 70.0 * Math.sqrt(Math.pow(this.currentStop.getLatitude() -  next.getLatitude(),2) + Math.pow((this.currentStop.getLongitude() - next.getLongitude()), 2));
        //
        time = 1 + (distance.intValue() * 60  / this.speedOfBus );


        return time;
    }

    //Added getter for Bus Speed to calculate system efficiency
    public Integer getSpeedOfBus() {
        return this.speedOfBus;
    }
    //Added getter for Initial Passenger Capacity to calculate system efficiency
    public Integer getInitialPassengerCapacity(){
        return this.initialPassengerCapacity;
    }

    // Setters

    public void setIsRunning(){ this.isRunning = Boolean.TRUE;}

    // two cases, steady state, and if a route change is pending.
    // change the ROUTE
    public void updateCurrentStop(){
        this.currentStop = this.myRoute.getNextStop(this.currentIndex);
        this.currentIndex = (this.currentIndex  + 1) % this.myRoute.getNumberOfStops();
    }

    public void updatePassengerCount(int passengerChange) { passengersOnBus = passengersOnBus + passengerChange; }
}
